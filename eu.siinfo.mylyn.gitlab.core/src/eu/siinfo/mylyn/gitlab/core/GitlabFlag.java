

package eu.siinfo.mylyn.gitlab.core;

public enum GitlabFlag {
	/**
	 * Sets the attribute to ReadOnly.
	 */
	READ_ONLY,

	/**
	 * Default Attribute
	 */
	ATTRIBUTE,

	/**
	 * A flag to indicate, that an attribute describes a person.
	 */
	PEOPLE;
}
